package pages;

import cucumber.api.java.en.And;
import libraries.Annotations1;

public class DuplicateLead extends Annotations1{

	@And("click the duplicate Tab")
	public DuplicateLead clickTheDuplicateTab() {
		return new DuplicateLead();

	}

	@And("click the create button")
	public DuplicateLead clickTheCreateButton() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		return this;

	}



}
