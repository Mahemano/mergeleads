package pages;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import libraries.Annotations1;

public class MergeLeads extends Annotations1{

	@When("Click Merge Leads Tab")
	public MergeLeads clickMergeLeadsTab() {
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
	    return this;
	}

	@And("Click on Icon near From Lead")
	public MergeLeads clickOnIconNearFromLead()  {
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    return this;

}

	@And("Click First Resulting lead")
	public MergeLeads clickFirstResultingLead() throws InterruptedException {
		
		Set<String> Windows = driver.getWindowHandles();

		for (String allvalues : Windows) {
			System.out.println(allvalues);

		}
		List<String> allwin = new ArrayList<String>();
		allwin.addAll(Windows);

		driver.switchTo().window(allwin.get(1));
		String txt= driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").getText();
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").sendKeys(txt);

		//driver.findElementByXPath("//button[@class='x-btn-text']").click();
	//	Thread.sleep(4000);
	driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(allwin.get(0));
		
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	return this;

	    	}

	@And("Click on Icon near To Lead")
	public MergeLeads clickOnIconNearToLead() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		return this;
 
	}
	
@And("Click Resulting lead")
	public MergeLeads clickFirstResultingLead1() throws InterruptedException {
	Set<String> Win1 = driver.getWindowHandles();

	for (String allvalues : Win1) {
	System.out.println(allvalues);

}
	List<String> allwin1 = new LinkedList<String>();
	allwin1.addAll(Win1);

	driver.switchTo().window(allwin1.get(1));
	Thread.sleep(4000);


	String sd= driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[10]").getText();
	System.out.println(sd);
	driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[10]").sendKeys(sd);


    driver.findElementByXPath("//button[@class='x-btn-text']").click();
	Thread.sleep(4000);
	driver.findElementByXPath("(//a[@class='linktext'])[10]").click();
driver.switchTo().window(allwin1.get(0));
return this; 

	}
	@When("Click on the Merge Lead Button")
	public MergeLeads clickOnTheMergeLeadButton() {
		driver.findElementByXPath("//a[text()='Merge']").click();
		return this;
	   
	}

	@And("click the ok button")
	public ViewLeads clickTheOkButton() throws InterruptedException {
		Alert alert=driver.switchTo().alert();
		alert.accept();
		return new ViewLeads();
	}

	
	



}
