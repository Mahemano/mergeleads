package pages;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import libraries.Annotations1;

public class ViewLeads extends Annotations1{

	
	@Then("Click on the viewleads Tab")
	public ViewLeads clickLeadsTab1() {
		driver.findElementById("viewLead_firstName_sp").click();
		return this;
	}

	@Then("Click on the Edit Tab")
	public EditLead editleadtab() {
		driver.findElementByLinkText("Edit").click();
		return new EditLead();
	}
	
	
	@And("click the duplicate Tab")
	public DuplicateLead duplead() {
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLead();
	}

	


	}
