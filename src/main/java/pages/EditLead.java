package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import libraries.Annotations1;

public class EditLead extends Annotations1{

	@And ("edit the company name")
	public EditLead editTheCompanyName() {
		WebElement ele=	driver.findElementById("updateLeadForm_companyName");
		ele.clear();
		ele.sendKeys("mahe");
		return this;
 
	}

	@And("click the update button")
	public ViewLeads clickTheUpdateButton() {
		driver.findElementByXPath("//input[@value='Update']").click();
		return new ViewLeads();

	}

}
