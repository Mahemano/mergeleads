package implementclass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EditLead;

public class Stepdefinition {
	
	public ChromeDriver driver;

	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver =new ChromeDriver();
	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
}

	@And("set the timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

	}

	@And("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");

	}

	@And("Enter the Username")
	public void enterTheUsername() {
		driver.findElementById("username").sendKeys("DemoSalesManager");

	}

	@And("Enter the password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("crmsfa");

	}

	@When("I click on the login button")
	public void iClickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();

	}

	@Then("I click the CRM\\/SFA")
	public void iClickTheCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();

	}
	
	@And("click the Leads")
	public void clickleads() {
		driver.findElementByLinkText("Leads").click();
	    
	}

	@When("Click Merge Leads Tab")
	public void clickMergeLeadsTab() {
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
	    
	}

	@And("Click on Icon near From Lead")
	public void clickOnIconNearFromLead()  {
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
}

	@And("Click First Resulting lead")
	public void clickFirstResultingLead() throws InterruptedException {
		
		Set<String> Windows = driver.getWindowHandles();

		for (String allvalues : Windows) {
			System.out.println(allvalues);

		}
		List<String> allwin = new ArrayList<String>();
		allwin.addAll(Windows);

		driver.switchTo().window(allwin.get(1));
		String txt= driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").getText();
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").sendKeys(txt);

		//driver.findElementByXPath("//button[@class='x-btn-text']").click();
	//	Thread.sleep(4000);
	driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(allwin.get(0));
		
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

	    	}

	@And("Click on Icon near To Lead")
	public void clickOnIconNearToLead() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
 
	}
	
@And("Click Resulting lead")
	public void clickFirstResultingLead1() throws InterruptedException {
	Set<String> Win1 = driver.getWindowHandles();

	for (String allvalues : Win1) {
	System.out.println(allvalues);

}
	List<String> allwin1 = new LinkedList<String>();
	allwin1.addAll(Win1);

	driver.switchTo().window(allwin1.get(1));
	Thread.sleep(4000);


	String sd= driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[10]").getText();
	System.out.println(sd);
	driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[10]").sendKeys(sd);


    driver.findElementByXPath("//button[@class='x-btn-text']").click();
	Thread.sleep(4000);
	driver.findElementByXPath("(//a[@class='linktext'])[10]").click();
driver.switchTo().window(allwin1.get(0));

	}
	@When("Click on the Merge Lead Button")
	public void clickOnTheMergeLeadButton() {
		driver.findElementByXPath("//a[text()='Merge']").click();

	   
	}

	@And("click the ok button")
	public void clickTheOkButton() throws InterruptedException {
		Alert alert=driver.switchTo().alert();
		alert.accept();
	}
	@Then("Click on the Edit Tab")
	public void clickOnTheEditTab() throws InterruptedException {
		Thread.sleep(2000);

		driver.findElementByLinkText("Edit").click();
}

	@And ("edit the company name")
	public void editTheCompanyName() {
		WebElement ele=	driver.findElementById("updateLeadForm_companyName");
		ele.clear();
		ele.sendKeys("mahe");
 
	}

	@And("click the update button")
	public void clickTheUpdateButton() {
		driver.findElementByXPath("//input[@value='Update']").click();


	}

	@And("click the duplicate Tab")
	public void clickTheDuplicateTab() {
		driver.findElementByLinkText("Duplicate Lead").click();

	}

	@And("click the create button")
	public void clickTheCreateButton() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();

	}




}
